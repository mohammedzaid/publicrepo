<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link   href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/bootstrap.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta http-equiv="imagetoolbar" content="no" />
    <title>EMR Med Services</title>
</head>
<body>

 <!-- Navigation -->
<?php require("navigation.php"); ?>
</ul>
        </div>
            <!-- /.navbar-collapse -->
    </div>
        <!-- /.container -->
</nav>
<!-- /.slider -->
 <header id="myCarousel" class="carousel slide">

        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <div class="item active">
                <div  class="fill" >
                    <img height="300px" width="100%" src="image/1.jpg"  >
                </div>
        </div>
            <div class="item">
                <div class="fill">
                    <img height="300px" width="100%" src="image/2.jpg"  >
                </div>
            </div>
            <div class="item">
                <div class="fill">
                    <img height="300px" width="100%" src="image/3.jpg"  >
                </div>
            </div>
        </div>
    </div>
    	  
        <!-- /container -->
   
    <div class="container">

        <div class="row">
            <h1 class="page-header">
                Welcome to EMR MED SERVICES
            </h1>
        <!-- The content of your page would go here. -->
        
        <div class="about-content">
            <h2>Centre of Excellence</h2>
            <p>Combining the best specialists and equipment to provide you nothing short of the best in healthcare . Our vision is simple – To provide the highest quality of service, assured pan-Indian diabetes care and to create a visible, healthy difference in your life .</p>
            <a href="aboutus.php" class="btn btn-success">About us</a>

        </div>

        </div>
   </div> 
 <!-- /container ends-->

  <!-- /footer begins -->



  <!-- /footer ends -->


<!-- jQuery -->

<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->

<script src="js/bootstrap.min.js"></script>

<!-- Script to Activate the Carousel -->

<script>
    $('.carousel').carousel({
        interval: 1800 //changes the speed
    })
</script>
</body>
</html>
