<?php
session_start();
	     $name=$_SESSION['name'];
	     $id=$_SESSION['id'];
             $address=$_SESSION['address'];
             $refby=$_SESSION['refby'];
             $mobile=$_SESSION['mobile'];
             $dob=$_SESSION['dob'];
             $date_report=$_SESSION['date_report'];
	     $age=$_SESSION['age'];
             $height=$_SESSION['height'];
             $weight=$_SESSION['weight'];
             $gender=$_SESSION['gender'];
             $diagnosis=$_SESSION['diagnosis'];
             $HgA1c_Date=$_SESSION['HgA1c_Date'];
             $HgA1c=$_SESSION['HgA1c'];
             $cholesterol_date=$_SESSION['cholesterol_date'];
             $LDL_c=$_SESSION['LDL_c'];
             $HDL=$_SESSION['HDL'];
             $Trig= $_SESSION['Trig'];
             $comments=$_SESSION['comments'];
             $interpretation=$_SESSION['interpretation'];
             $test=$_SESSION['test'];
             $Hospital=$_SESSION['Hospital'];


//============================================================+
// File name   : diabetesrep.php
// Begin       : 2008-03-04
// Last Update : 2016-10-21
//
// Description : Example 006 for TCPDF class
//               WriteHTML and RTL support
//
// Author: CK
//
// (c) Copyright:
//               CK
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: WriteHTML and RTL support
 * @author CK
 * @since 2016-10-04
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');
class MYPDF extends TCPDF {


	// Page footer
	public function Footer() {
		// Position at 15 mm from bottom
		$this->SetY(-15);
		// Set font
		$this->SetFont('helvetica', 'I', 8);
		// Page number
		$this->Cell(0, 10, 'EMR MED SERVICES', 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
}
// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('CK');
$pdf->SetTitle('DIABETES REPORT');
$pdf->SetSubject('REPORT GENERATION');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
$image=$_SESSION['logo'];

// set default header data
$pdf->SetHeaderData("$image", PDF_HEADER_LOGO_WIDTH, "$Hospital", "Laboratory Testing in $test
 Examined By : $refby");

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 10);

// add a page
$pdf->AddPage();


// writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)

// create some HTML content



// output some RTL HTML content
$html  = '<h3 style="color:#4863A0;" align="center">Laboratory Investigation </h3><br>
<table border="0" cellspacing="15" cellpadding="10">
    <tr>
		<th align="center">Patient Name </th>
		<td align="center">'.$name.'</td>
		<th align="center">Age</th>
		<td align="center">'.$age.'</td>
		
	</tr>
	<tr>
		<th align="center">Patient Address</th>
		<td align="center">'.$address.'</td>
		<th align="center">Date of Report</th>
		<td align="center">'.$date_report.'</td>
	</tr>
	<tr>
		<th align="center">Ref By</th>
		<td align="center">'.$refby.'</td>
		
	</tr>
	
	</tr>
</table>
    <br/> 
    <h3 style="color:#4863A0;" align="center">Diabetes Function Tests</h3>
    <h4 style="color:#4863A0;" align="center">Detailed Report</h4><br>

    <table border="0" cellspacing="10" cellpadding="1">
	<tr>
		<th>Test</th>
		<th align="center">Values</th>
		<th align="center">Units</th>
		<th align="center">Normal Range</th>
	</tr>
	
	<tr>
		<th>LDL-c </th>
		<th align="center">'.$LDL_c.'</th>
		
	</tr>
	<tr>
		<th>HDL  </th>
		<th align="center">'.$HDL.'</th>
		
	</tr>
	<tr>
		<th>Trig   </th>
		<th align="center">'.$Trig.'</th>
	</tr>
	<tr>
		<th>HgA1c  </th>
		<th align="center">'.$HgA1c.'</th>
		
	</tr>
	</table>
	<br>
	<h3 style="color:#4863A0;" align="left">Comments : </h3> '.$comments.' <br><br>
	<h3 style="color:#4863A0;" align="left">Interpretation :</h3> '.$interpretation.'  <br><br>
	<h3 style="color:#4863A0;" align="left">Signature :</h3> ___________________________ <br><br>

';


// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Print a table

//Close and output PDF document
$pdf->Output('patient.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+






