<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="footer, address, phone, icons" />
    <title>EMR Med Services</title>
    <link rel="stylesheet" href="css/demo.css">
    <link rel="stylesheet" href="css/footer-distributed-with-address-and-phones.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
    <link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
</head>
<body>
<!-- The content of your page would go here. -->
  <footer class="footer-distributed">
    
    <div class="footer-left">

        <h3>Company<span>logo</span></h3>

        <p class="footer-links">
        
          <a href="index.php">Home</a>
          ·
          <a href="aboutus.php">About Us</a>
          ·
          <a href="#">Services</a>
          ·
          <a href="#">Faq</a>
          ·
          <a href="indexlog.php">Login</a>
          ·
          <a href="contactus.php">Contact</a>
        </p>

        <p class="footer-company-name">EMR Med Services &copy; 2016</p>
      </div>

      <div class="footer-center">

        <div>
          <i class="fa fa-map-marker"></i>
          <p><span>#6,Maruthi Mansion </span> Koramangala , Bangalore</p>
        </div>

        <div>
          <i class="fa fa-phone"></i>
          <p>+91-9986720343</p>
        </div>

        <div>
          <i class="fa fa-envelope"></i>
          <p><a href="mailto:info@smartideasinc.in">info@smartideasinc.in</a></p>
        </div>

      </div>

      <div class="footer-right">

        <p class="footer-company-about">
          <span>About the company</span>
         Our objective is to maintain the leadership position in establishment of hospitals, medical diagnostic services to our customers.
        </p>

        <div class="footer-icons">

          <a href="#"><i class="fa fa-facebook"></i></a>
          <a href="#"><i class="fa fa-twitter"></i></a>
          <a href="#"><i class="fa fa-linkedin"></i></a>
          <a href="#"><i class="fa fa-github"></i></a>

        </div>

      </div>

    </footer>

  </body>

</html>
