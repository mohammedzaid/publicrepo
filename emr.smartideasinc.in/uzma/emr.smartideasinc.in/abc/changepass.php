<?php
require 'database-config.php';
session_start();

$code = isset($_POST['code']) ? $_POST['code'] : '';

 $email = isset($_SESSION['sess_mail']) ? $_SESSION['sess_mail'] : '';
  $password = isset($_POST['password1']) ? $_POST['password1'] : '';
$password = isset($_POST['cpassword']) ? $_POST['cpassword'] : '';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>EMR MED REPORT</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
       
        <!-- Font -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,700,600italic,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
         <!-- Font -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/responsive.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
  </head>


  <body>
  <div class="container">
      <div class="row">
      <h3 align="left">EMR MED SERVICES</h3>
      </div>
      <div class="col-md-6 col-md-offset-3">
      <div class="block-margin-top">
      <form name="cpass" method=POST name="contact" action="changepas_exec.php">
      <div>

      <label for="code">Your Email Address </label>
      <input type="email" class="input-lg form-control" placeholder="<?php echo $email ?>" name="txtemail" disabled >
      </div>

      <div>
      <label for="code">Enter your Code </label>
      <input type="password" class="input-lg form-control" name="code" id="code" placeholder="unique code" required >
      </div>

      <div>
      <label for="password">New Password:</label>
      <input type="password" class="input-lg form-control" name="password1" id="password1" placeholder="new Password" required >
      </div>

      <div>
      <label for="password">Repeat Password:</label>
      <input type="password" class="input-lg form-control" name="cpassword" id="cpassword" placeholder="Repeat Password" required >
      </div>
      <br>

      <input type="submit" class="btn btn-lg btn-primary btn-block" data-loading-text="Changing Password..." value="Change Password">
      <hr>
      </form>
      </div>
      </div>
      </div>
      </div>



<br>




    <!-- Copyright -->
    <div class="copyright">
        <div class="container">
          <div class="row">
            <div class="col-sm-4">
              <div class="copy-text">
                <p>All Rights Reserved | Copyright 2016 © <strong><a href="http://www.pfind.com/goodies/bizium/">EMR Med Services</a></strong> Developed By <strong><a href="http://smartideasinc.in/">SmartIdeas Inc</a></strong></p>
              </div>
            </div>
            <div class="col-sm-5">
              <div class="footer-menu pull-right">
                <ul>
                  <li><a href="#">Home</a></li>
                  <li><a href="#">About</a></li>
                  <li><a href="#">Services</a></li>
                  <li><a href="#">Faq</a></li>
                  <li><a href="#">Pricing</a></li>
                  <li><a href="#">Blog</a></li>
                  <li><a href="#">Contact</a></li>
                </ul>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="social">
                <ul>
                  <li><a href="https://www.facebook.com/smartideasinc/"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="https://twitter.com/smartideas013"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="https://www.linkedin.com/company/smart-ideas-inc"><i class="fa fa-linkedin"></i></a></li>
                  <li><a href="info@smartideasinc.in"><i class="fa fa-google-plus"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    
    <!-- footer -->
  </body>
 </html>

