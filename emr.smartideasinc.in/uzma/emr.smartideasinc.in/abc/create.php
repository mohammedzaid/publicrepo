<?php 
	session_start();
        require 'database-config.php';
        
            $link = mysql_connect($host, $user, $pass);
    if(!$link) {
        die('Failed to connect to server: ' . mysql_error());
    }
    
    //Select database
    $db = mysql_select_db($database);
    if(!$db) {
        die("Unable to select database");

    }
       $Hospital=$_SESSION['sess_Hospital'];
       $z=$_SESSION['sess_logo'];
       $role=$_SESSION['sess_userrole'];


          $sql2 = "SELECT * FROM login where Hospital ='".$Hospital."' and role='doctor'";


$result=mysql_query($sql2, $link);
$count=mysql_num_rows($result);





	if ( !empty($_POST)) {
		// keep track validation errors
		$nameError = null;
		$addressError = null;
		$mobileError = null;
		$dobError = null;
		$date_reportError = null;
		$heightError = null;
		$weightError = null;
		$ageError = null;
		$genderError = null;
		$refbyError = null;
		$diseaseError = null;

		
		// keep track post values
		$name = $_POST['name'];
		$address = $_POST['address'];
		$mobile = $_POST['mobile'];
		$dob = $_POST['dob'];
		$date_report = $_POST['date_report'];
		$height = $_POST['height'];
		$weight = $_POST['weight'];
		$age = $_POST['age'];
		$gender = $_POST['gender'];
		$refby = $_POST['refby'];
		$disease = $_POST['disease'];
		
		
		
		// validate input
		$valid = true;
		if (empty($name)) {
			$nameError = 'Please enter Name';
			$valid = false;
		}
		
		if (empty($address)) {
			$addressError = 'Please enter Address';
			$valid = false;
		}
		
		if (empty($mobile)) {
			$mobileError = 'Please enter Mobile Number';
			$valid = false;
		}

		if (empty($dob)) {
			$dobError = 'Please enter Date Of Birth';
			$valid = false;
		}

		if (empty($date_report)) {
			$date_reportError ='Please enter Date of Report';
			$valid = false;
		}

		if (empty($height)) {
			$heightError = 'Please enter Height';
			$valid = false;
		}

		if (empty($weight)) {
			$weightError = 'Please enter Weight';
			$valid = false;
		}

		if (empty($age)) {
			$ageError = 'Please enter Age';
			$valid = false;
		}


		if (empty($refby)) {
			$refbyError = 'select the doctor';
			$valid = false;
		}
		if (empty($disease)) {
			$diseaseError = 'select the disease of the patient';
			$valid = false;
		}

		
	require 'database.php';

				
		// insert data
		if ($valid) {

			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "INSERT INTO patient (name,address,refby,mobile,dob,date_report,height,weight,age,gender,logo,disease,Hospital) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
			$q = $pdo->prepare($sql);
			$q->execute(array($name,$address,$refby,$mobile,$dob,$date_report,$height,$weight,$age,$gender,$z,$disease,$Hospital));
			Database::disconnect();
			header("Location: hospital_home.php");
		}
	}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    	<meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>EMR MED REPORT</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
       
        <!-- Font -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,700,600italic,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
         <!-- Font -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/responsive.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        <title> EMR Med Report</title>
</head>

<body>

<!-- Navigation -->
<div class="main-menu">
                <div class="navbar-wrapper">
                    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                        <div class="container">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle Navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                
                                <a href="index.html" class="navbar-brand"><img src="img/logo.png" alt="Logo" /></a>                          
                            </div>
                            
                            <div class="navbar-collapse collapse">
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="index.html">Home</a></li>
                                    <li><a href="patient_detail1.php">Doctor Home</a></li>
                                    <li><a href="logout.php">Logout</a></li>
                                </ul>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
 

<!-- Navigation ends -->
<br>
<br>
    
    
    <div class="container">
      
      <div class="col-md-6 col-md-offset-3">

                  <h4></span>New Patient Record   <span class="glyphicons glyphicons-user-add"></h4>
                  <br/>
                            <div class="block-margin-top">
    			<form class="form-horizontal" action="create.php" method="post">
					  <div class="control-group <?php echo !empty($nameError)?'error':'';?>">
					    <label class="control-label">Name</label>
					    <div class="controls">
					      	<input name="name" type="text"  placeholder="Name" value="<?php echo !empty($name)?$name:'';?>">
					      	<?php if (!empty($nameError)): ?>
					      		<span class="help-inline"><?php echo $nameError;?></span>
					      	<?php endif; ?>
					    </div>
					  </div>
					  <div class="control-group <?php echo !empty($addressError)?'error':'';?>">
					    <label class="control-label">Patient Address</label>
					    <div class="controls">
					      	<input name="address" type="text" placeholder="Address" value="<?php echo !empty($address)?$address:'';?>">
					      	<?php if (!empty($addressError)): ?>
					      		<span class="help-inline"><?php echo $addressError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($mobileError)?'error':'';?>">
					    <label class="control-label">Mobile Number</label>
					    <div class="controls">
					      	<input name="mobile" type="text"  placeholder="Mobile Number" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" value="<?php echo !empty($mobile)?$mobile:'';?>">
					      	<?php if (!empty($mobileError)): ?>
					      		<span class="help-inline"><?php echo $mobileError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>
					  <div class="control-group <?php echo !empty($dobError)?'error':'';?>">
					    <label class="control-label">Date Of Birth</label>
					    <div class="controls">
					      	<input name="dob" type="date"  placeholder="Date Of Birth" value="<?php echo !empty($dob)?$dob:'';?>">
					      	<?php if (!empty($dobError)): ?>
					      		<span class="help-inline"><?php echo $dobError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>
					  <div class="control-group <?php echo !empty($date_reportError)?'error':'';?>">
					    <label class="control-label">Date Of Report</label>
					    <div class="controls">
					      	<input name="date_report" type="date"  placeholder="Date Of Report" value="<?php echo !empty($date_report)?$date_report:'';?>">
					      	<?php if (!empty($date_reportError)): ?>
					      		<span class="help-inline"><?php echo $date_reportError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>
					  <div class="control-group <?php echo !empty($heightError)?'error':'';?>">
					    <label class="control-label">Height</label>
					    <div class="controls">
					      	<input name="height" type="text"  placeholder="Height" value="<?php echo !empty($height)?$height:'';?>">
					      	<?php if (!empty($heightError)): ?>
					      		<span class="help-inline"><?php echo $heightError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>
					  <div class="control-group <?php echo !empty($weightError)?'error':'';?>">
					    <label class="control-label">Weight</label>
					    <div class="controls">
					      	<input name="weight" type="text"  placeholder="Weight" value="<?php echo !empty($weight)?$weight:'';?>">
					      	<?php if (!empty($weightError)): ?>
					      		<span class="help-inline"><?php echo $weightError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>
					  <div class="control-group <?php echo !empty($ageError)?'error':'';?>">
					    <label class="control-label">Age</label>
					    <div class="controls">
					      	<input name="age" type="text"  placeholder="Age" value="<?php echo !empty($age)?$age:'';?>">
					      	<?php if (!empty($ageError)): ?>
					      		<span class="help-inline"><?php echo $ageError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>
					  <div class="control-group <?php echo !empty($genderError)?'error':'';?>">
					    <label class="control-label">Gender</label>
					    <div class="controls">
					      	
  <select style="width: 150px" name="gender" value="gender">
    <option value="Male">Male</option>
    <option value="Female">Female</option>
    <option value="Other">Other</option>
    
  </select>

					    </div>
					  </div>
					  <div class="control-group <?php echo !empty($refbyError)?'error':'';?>">
					    <label class="control-label">Ref By</label>
					    <div class="controls">
					    <select style="width: 150px;" name="refby">
					            <option value="">Select...</option>
					            <?php while($resource=mysql_fetch_array($result)){ ?>

					            <option style="width: 150px;" value="<?php echo $resource[6] ?>"><?php echo $resource[6] ?></option>
					<?php } ?>

					        </select>
					    </div>
					    </div>
					    <div class="control-group <?php echo !empty($diseaseError)?'error':'';?>">
					    <label class="control-label">Disease</label>
					    <div class="controls">
					       <select style="width: 150px;" name="disease">
  <option value="Diabetes">Diabetes</option>
  <option value="Thyroid">Thyroid</option>
 
</select>
					    </div></div>

							<br>
						<div class="form-actions">
						  <button type="submit" class="btn btn-success">Create</button>
						  <a class="btn btn-success" href="hospital_home.php">Back</a>
						</div>
					</form>
				</div>
				
    </div>
</div>
</div> 
<!-- /container -->
<br>
<br>
<br>
<div class="copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="copy-text">
                                <p>All Rights Reserved | Copyright 2016 © <strong><a href="http://www.pfind.com/goodies/bizium/">EMR Med Services</a></strong> Developed By <strong><a href="http://smartideasinc.in/">SmartIdeas Inc</a></strong></p>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="footer-menu pull-right">
                                <ul>
                                    <li><a href="index.html">Home</a></li>
                                    <li><a href="#">About</a></li>
                                    <li><a href="#">Services</a></li>
                                    <li><a href="#">Faq</a></li>
                                    <li><a href="#">Pricing</a></li>
                                    <li><a href="#">Blog</a></li>
                                    <li><a href="#">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="social">
                                <ul>
                                    <li><a href="https://www.facebook.com/smartideasinc/"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="https://twitter.com/smartideas013"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="https://www.linkedin.com/company/smart-ideas-inc"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="info@smartideasinc.in"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
        <!-- footer -->
  </body>
</html>
