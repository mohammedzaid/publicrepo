<?php 
	require 'database.php';
	$id = null;
	if ( !empty($_GET['id'])) {
		$id = $_REQUEST['id'];
	}
	
	if ( null==$id ) {
		header("Location: patient_detail.php");
	} else {
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "SELECT * FROM patient where id = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($id));
		$data = $q->fetch(PDO::FETCH_ASSOC);
		Database::disconnect();
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>EMR MED REPORT</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Font -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,700,600italic,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
         <!-- Font -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/responsive.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        <title> EMR Med Report</title>
</head>

<body>
<!-- Navigation -->
<div class="main-menu">
                <div class="navbar-wrapper">
                    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                        <div class="container">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle Navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                
                                <a href="#" class="navbar-brand"><img src="img/logo.png" alt="Logo" /></a>                          
                            </div>
                            
                            <div class="navbar-collapse collapse">
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="index.html">Home</a></li>
                                    <li><a href="patient_detail1.php">Doctor Home</a></li>
                                    <li><a href="indexlog.php">Logout</a></li>
                                </ul>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
 

<!-- Navigation ends -->
<br>
<br>
    <div class="container">
    
    			<div>
    				<div class="row">
		    			<h3>Read a Patient Detail</h3>
		    		</div>
		    		
	    			<div class="form-horizontal" >
	    			<table style="width:100%">
					  <tr>
					    <th>Field</th>
					    <th>Value</th>
						<th>Field</th>
					    <th>Value</th>
					</tr>
					  <tr>
					    <th>Name :</th>
					    <td><?php echo $data['name'];?></td>
					    <th>Address :</th>
					    <td><?php echo $data['address'];?></td>
					  </tr>
					  <tr>
					    <th>Ref By :</th>
					    <td><?php echo $data['refby'];?></td>
					    <th>Mobile Number</th>
					    <td><?php echo $data['mobile'];?></td>
					  </tr>
					    <tr>
					    <th>Date Of Birth</th>
					    <td><?php echo $data['dob'];?></td>
					    <th>Date Of Report</th>
					    <td><?php echo $data['date_report'];?></td>
					  </tr>
					    <tr>
					    <th>Height</th>
					    <td><?php echo $data['height'];?></td>
					    <th>Weight</th>
					    <td><?php echo $data['weight'];?></td>
					  </tr>
					    <tr>
					    <th>Age :</th>
					    <td><?php echo $data['age'];?></td>
					    <th>Gender</th>
					    <td><?php echo $data['gender'];?></td>
					  </tr>
					    <tr>
					    <th>Diagnosis</th>
					    <td><?php echo $data['diagnosis'];?></td>
					    <th>HgA1c_Date</th>
					    <td><?php echo $data['HgA1c_Date'];?></td>
					  </tr>
					    <tr>
					    <th>HgA1c</th>
					    <td><?php echo $data['HgA1c'];?></td>
					    <th>Cholesterol_date</th>
					    <td><?php echo $data['cholesterol_date'];?></td>
					  </tr>

					    <tr>
					    <th>LDL_c</th>
					    <td><?php echo $data['LDL_c'];?></td>
					    <th>HDL</th>
					    <td><?php echo $data['HDL'];?></td>
					  </tr>
					    <tr>
					    <th>Trig</th>
					    <td><?php echo $data['Trig'];?></td>
					    <th>Test Assigned</th>
					    <td><?php echo $data['test'];?></td>
					  </tr>
					    <tr>
					    <th>Comments</th>
					    <td><?php echo $data['comments'];?></td>
					    
					  </tr>
					    <tr>
					    <th>Interpretation</th>
					    <td><?php echo $data['interpretation'];?></td>
					    
					  </tr>
					    
					</table>
         <div class="form-actions">
		<a class="btn btn-success" href="admin_home.php">Back</a>
	</div>
</div>
</div>
				
    </div> <!-- /container -->



<!-- Copyright -->
  <footer class="site-footer">
    <div class="copyright">
        <div class="container">
          <div class="row">
            <div class="col-sm-4">
              <div class="copy-text">
                <p>All Rights Reserved | Copyright 2016 © <strong><a href="http://emr.smartideasinc.in/">EMR Med Services</a></strong> Developed By <strong><a href="http://smartideasinc.in/">SmartIdeas Inc</a></strong></p>
              </div>
            </div>
            <div class="col-sm-5">
              <div class="footer-menu pull-right">
                <ul>
                  <li><a href="index.html">Home</a></li>
                  <li><a href="#about">About</a></li>
                  <li><a href="#features">Services</a></li>
                  <li><a href="#faq">Faq</a></li>
                  <li><a href="#pricing">Pricing</a></li>
                  <li><a href="#blog">Blog</a></li>
                  <li><a href="#contact-us">Contact</a></li>
                </ul>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="social">
                <ul>
                  <li><a href="https://www.facebook.com/smartideasinc/"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="https://twitter.com/smartideas013"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="https://www.linkedin.com/company/smart-ideas-inc"><i class="fa fa-linkedin"></i></a></li>
                  <li><a href="info@smartideasinc.in"><i class="fa fa-google-plus"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
  </footer>
<!-- footer -->


  </body>
</html>
