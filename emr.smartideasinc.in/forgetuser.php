<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>EMR MED REPORT</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
       
        <!-- Font -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,700,600italic,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
         <!-- Font -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/responsive.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        <title> EMR Med Report</title>
</head>
<body >
      <div class="container">
      <div class="row">
      <h3 align="left">EMR MED SERVICES</h3>
      </div>
      <div class="col-md-6 col-md-offset-3">
      <br>
      <h4 class="form-signin-heading">Forgot Password ?</h2>
      <br/>
      <div class="block-margin-top">
     <!--  forgot username inputs -->
    <form action="usermail.php" method="POST" class="form-signin col-md-8 col-md-offset-2" role="form">  
     <input type="email" class="input-block-level" placeholder="Email address" name="txtemail" required />
           <br><br>
         <button class="btn btn-danger btn-primary" type="submit" name="email">Generate new Password</button>
         </form>
          </div>
         </div>

              </div>
            
           </div>


<!-- Copyright -->
  <footer class="site-footer">
    <div class="copyright">
        <div class="container">
          <div class="row">
            <div class="col-sm-4">
              <div class="copy-text">
                <p>All Rights Reserved | Copyright 2016 © <strong><a href="http://emr.smartideasinc.in/">EMR Med Services</a></strong> Developed By <strong><a href="http://smartideasinc.in/">SmartIdeas Inc</a></strong></p>
              </div>
            </div>
            <div class="col-sm-5">
              <div class="footer-menu pull-right">
                <ul>
                  <li><a href="index.html">Home</a></li>
                  <li><a href="#about">About</a></li>
                  <li><a href="#features">Services</a></li>
                  <li><a href="#faq">Faq</a></li>
                  <li><a href="#pricing">Pricing</a></li>
                  <li><a href="#blog">Blog</a></li>
                  <li><a href="#contact-us">Contact</a></li>
                </ul>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="social">
                <ul>
                  <li><a href="https://www.facebook.com/smartideasinc/"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="https://twitter.com/smartideas013"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="https://www.linkedin.com/company/smart-ideas-inc"><i class="fa fa-linkedin"></i></a></li>
                  <li><a href="info@smartideasinc.in"><i class="fa fa-google-plus"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
  </footer>
<!-- footer -->



    <script src="bootstrap/js/jquery-1.9.1.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>


 
