-- MySQL dump 10.13  Distrib 5.5.53, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: medical
-- ------------------------------------------------------
-- Server version	5.5.53-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `mail_id` varchar(40) NOT NULL,
  `Hospital` varchar(30) NOT NULL,
  `Diease` varchar(20) NOT NULL,
  `logo` varchar(20) NOT NULL,
  `role` varchar(100) NOT NULL,
  `code` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login`
--

LOCK TABLES `login` WRITE;
/*!40000 ALTER TABLE `login` DISABLE KEYS */;
INSERT INTO `login` VALUES (1,'shafi','bangalore','9036931082','admin','uzma','uzma.iqbal@smartideasinc.in','NA','','in.jpg','admin','kzjNu3vD'),(2,'doctor','bangalore','9036931082','doctor','ZAID','mhdzaid17@gmail.com','zaid','','logo_example.jpg','doctor','EjstFAlS'),(3,'chaithra','bangalore','9036931082','abc','aaa','chaithra.kashyap@smartideasinc.in','zaid','','logo_example.jpg','doctor','pZmiVNt8'),(5,'boring','shivaginagar','9036931082','boring','uzma','uzma.iqbal@smartideasinc.in','boring','diabetes','bilal3.jpg','Hospital','kzjNu3vD'),(6,'UZMA','BANGALORE','9036931082','uzma','uzma','uzma.iqbal@smartideasinc.in','boring12','NA','a.jpg','Hospital','kzjNu3vD'),(7,'UZMA','BANGALORE','9036931082','123','123','uzmaiqbalh@gmail.com','boring12','Diabetes','new_font.png','doctor','123451'),(8,'shafi','nagwar','8553947551','zaid','ZAID','mhdzaid17@gmail.com','governament','NA','in.jpg','Hospital','EjstFAlS'),(9,'UZMA','BANGALORE','9036931082','zaid','zaid','uzma.iqbal@smartideasinc.in','hospital','NA','m1.jpg','Hospital','123451'),(10,'UZMA','BANGALORE','9036931082','chaithra','chaithra','uzma.iqbal@smartideasinc.in','governament','Diabetes','a.jpg','doctor','123451'),(11,'Alina','bangalore','7795705526','DOC1','Chinnu','alina.tomy@smartideasinc.in','governament','diabetics','in.jpg','doctor','123451');
/*!40000 ALTER TABLE `login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient`
--

DROP TABLE IF EXISTS `patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `refby` varchar(50) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `date_report` date NOT NULL,
  `height` varchar(30) NOT NULL,
  `weight` varchar(30) NOT NULL,
  `age` int(10) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `disease` varchar(30) NOT NULL,
  `Hospital` varchar(40) NOT NULL,
  `diagnosis` varchar(50) NOT NULL,
  `HgA1c_Date` date NOT NULL,
  `HgA1c` float(5,3) NOT NULL,
  `cholesterol_date` date NOT NULL,
  `LDL_c` int(10) NOT NULL,
  `HDL` int(10) NOT NULL,
  `Trig` int(10) NOT NULL,
  `comments` varchar(300) NOT NULL,
  `interpretation` varchar(500) NOT NULL,
  `mail_id` varchar(40) NOT NULL,
  `logo` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient`
--

LOCK TABLES `patient` WRITE;
/*!40000 ALTER TABLE `patient` DISABLE KEYS */;
INSERT INTO `patient` VALUES (6,'Ahana Shanbog','#301,Tata Berlon Villa,MG Road,Bangalore-008','','9443256780','1994-10-20','2016-09-10','150cm','50kg',25,'Female','diabetes','','Diabetes,Dyslipidemia','2016-09-12',12.900,'2016-09-12',190,140,330,'','','mhdzaid17@gmail.com','logo_example.jpg'),(7,'Bharath','#207,Bommanahalli ,Bangalore','','9446524780','1990-12-10','2016-05-25','166cm','60kg',25,'Male','','','Diabetes,Dyslipidemia','2016-09-12',11.500,'2016-09-12',180,160,280,'','','zaid1710@gmail.com',''),(9,'Sanjeeda','#109,Tata Meskon Villa,Hosur Road,Bangalore-009','','9443256780','1990-12-10','2016-05-25','145cm','48kg',25,'Female','','','Diabetes,Dyslipidemia','2016-05-30',10.600,'2016-05-30',180,160,261,'','','mhdzaid17@gmail.com',''),(10,'Nia ','#109,Tata Meskon Villa,Hosur Road,Bangalore-009','','9443256780','1990-12-10','2016-09-10','140cm','50kg',25,'Female','','','Diabetes,Dyslipidemia','2016-09-12',12.900,'2016-09-12',180,200,260,'','','zaid1710@gmail.com',''),(11,'Nia ','#109,Tata Meskon Villa,Hosur Road,Bangalore-009','','9443256780','1990-12-10','2016-09-10','140cm','50kg',25,'Female','','','Diabetes,Dyslipidemia','2016-09-12',12.900,'2016-09-12',180,200,260,'','','mhdzaid17@gmail.com',''),(12,'Ishara','#109,Tata Meskon Villa,Hosur Road,Bangalore-009','','9443256780','1991-06-20','2016-05-25','140cm','45',24,'Female','','','Diabetes,Dyslipidemia','2016-09-12',10.600,'2016-09-12',180,160,255,'','','zaid1710@gmail.com',''),(13,'Ishara','#109,Tata Meskon Villa,Hosur Road,Bangalore-009','','9443256780','1991-06-20','2016-05-25','140cm','45',24,'Female','','','Diabetes,Dyslipidemia','2016-09-12',10.600,'2016-09-12',180,160,255,'','','mhdzaid17@gmail.com',''),(14,'Ameer','#207,Bommanahalli ,Bangalore','Dr.Sriram Bhat','9443256783','2016-10-06','2016-10-06','55','878',18,'Male','','','kadsaks','2016-10-06',0.000,'2016-10-25',190,199,255,'Dyslipidemia State , Please Corelate with the clinical findings','Interpretation of glycosuria in the teenage diabetic patient.','zaid1710@gmail.com',''),(15,'Ziva','#109,Tata Meskon Villa,Hosur Road,Bangalore-009','Dr. Aman Tinde','9443256780','1990-12-10','2016-09-10','160cm','50kg',25,'Female','','','Diabetes,Dyslipidemia','2016-09-12',12.900,'2016-09-12',180,2,261,'Dyslipidemia State , Please Corelate with the clinical findings','Interpretation of glycosuria in the teenage diabetic patient.','mhdzaid17@gmail.com',''),(16,'Preetham Malhotra','#301,Tata Berlon Villa,MG Road,Bangalore-008','Dr. Aman Tinde','9443256780','1991-06-20','2016-09-10','145 cm','52kg',24,'Male','','','Diabetes,Dyslipidemia','2016-09-12',11.600,'2016-06-12',190,160,255,'Dyslipidemia State , Please Corelate with the clinical findings','Interpretation of glycosuria in the teenage diabetic patient.','zaid1710@gmail.com',''),(17,'Shaan','#301,Tata Berlon Villa,MG Road,Bangalore-008','Dr. Aman Tinde','9443256780','1995-12-12','2016-09-10','120','36',15,'Male','','','Diabetes,Dyslipidemia','2016-09-12',10.600,'2016-06-12',155,145,233,'Dyslipidemia State , Please Corelate with the clinical findings','Interpretation of glycosuria in the teenage diabetic patient.','mhdzaid17@gmail.com',''),(18,'soumya shetty1','kudradi house kottara chowki mangalore','Dr.Manan','9871287887878','1991-12-12','1212-12-12','140cm','45kg',24,'Female','','','Diabetes','1890-12-12',99.999,'2878-12-06',123,12,12,'sdjhjSDJC','sdfd','zaid1710@gmail.com',''),(19,'UZMA','BANGALORE','uzmaiqbalh@gmail.com','9036931082','1990-11-12','2016-11-11','145','56',24,'female','Diabetes','boring12','','0000-00-00',0.000,'0000-00-00',0,0,0,'','','','a.jpg'),(20,'UZMA','BANGALORE','uzma.iqbal@smartideasinc.in','9036931082','1990-11-12','2016-11-11','145','56',24,'female','Diabetes','governament','','0000-00-00',0.000,'0000-00-00',0,0,0,'','','','in.jpg'),(21,'Chaitra','Chennai','alina.tomy@smartideasinc.in','7894561','2016-11-10','2016-11-11','5','50',20,'female','Diabetes','governament','','0000-00-00',0.000,'0000-00-00',0,0,0,'','','','in.jpg');
/*!40000 ALTER TABLE `patient` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-14 13:58:14
