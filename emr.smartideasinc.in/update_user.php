<?php 

session_start();
	require 'database-config.php';
        
            $link = mysql_connect($host, $user, $pass);
    if(!$link) {
        die('Failed to connect to server: ' . mysql_error());
    }
    
    //Select database
    $db = mysql_select_db($database);
    if(!$db) {
        die("Unable to select database");

    }
 
	require 'database.php';

	$id = null;
	if ( !empty($_GET['id'])) {
		$id = $_REQUEST['id'];
	}
	
	if ( null==$id ) {
		header("Location: admin_home.php");
	}
	
	if ( !empty($_POST)) {
		// keep track validation errors
		$nameError = null;
		$addressError = null;
		$phoneError = null;
		$mailError = null;
		$HospitalError = null;
		$DieaseError = null;
		$roleError = null;

		
		// keep track post values
		$name = $_POST['name'];
		$address = $_POST['address'];
		$phone = $_POST['phone'];
		$mail = $_POST['mail_id'];
		$Hospital = $_POST['Hospital'];
		$Diease = $_POST['Diease'];
		$role = $_POST['role'];
		
		
		
		
		// validate input
		$valid = true;
		if (empty($name)) {
			$nameError = 'Please enter Name';
			$valid = false;
		}
		
		if (empty($address)) {
			$addressError = 'Please enter address Address';
			$valid = false;
		} 

		if (empty($phone)) {
			$phoneError = 'Please enter Mobile Number';
			$valid = false;
		}
		
		if (empty($mail)) {
			$mailError = 'Please enter Mail Id';
			$valid = false;
		}

		if (empty($Hospital)) {
			$HospitalError = 'Please enter Hospital Name';
			$valid = false;
		}

		if (empty($Diease)) {
			$DieaseError = 'Please enter Disease Handled';
			$valid = false;
		}

		if (empty($role)) {
			$roleError = 'Please enter role';
			$valid = false;
		}

		


		
		// update data
		if ($valid) {
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE login  set name = ?, address = ?, phone = ?,mail_id =?, Hospital =?, Diease =?, role =?, WHERE id = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($name,$address,$phone,$mail_id,$Hospital,$Diease,$role,$id));
			Database::disconnect();
			header("Location: admin_home.php");
		}
	} else {
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "SELECT * FROM login where id = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($id));
		$data = $q->fetch(PDO::FETCH_ASSOC);
		$name = $data['name'];
		$address = $data['address'];
		$phone = $data['phone'];
		$mail = $data['mail_id'];
		$Hospital = $data['Hospital'];
		$Diease = $data['Diease'];
		$role = $data['role'];
		
		
		Database::disconnect();
	}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>EMR MED REPORT</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->
        
        <!-- Font -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,700,600italic,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <!-- Font -->
        
        
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/responsive.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
</head>

<body>
<!-- Navigation -->
<div class="main-menu">
                <div class="navbar-wrapper">
                    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                        <div class="container">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle Navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                
                                <a href="#" class="navbar-brand"><img src="img/logo.png" alt="Logo" /></a>                          
                            </div>
                            
                            <div class="navbar-collapse collapse">
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="index.html">Home</a></li>
                                    <li><a href="admin_home.php">Admin Home</a></li>
                                    <li><a href="patient.php">Patient Home</a></li>
                                    <li><a href="indexlog.php">Logout</a></li>
                                </ul>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
 

<!-- Navigation ends -->

<br>
<br>
    <div class="container">
     
      <div class="col-md-6 col-md-offset-3">

                  <h4></span>Update a Patient Detail    <span class="glyphicon glyphicon-user"></h4>
                  <br/>
                            <div class="block-margin-top">
    		
	    			<form class="form-horizontal" action="update_user.php?id=<?php echo $id?>" method="post">
					  <div class="control-group <?php echo !empty($nameError)?'error':'';?>">
					    <label class="control-label">Name</label>
					    <div class="controls">
					      	<input name="name" type="text"  placeholder="Name" value="<?php echo !empty($name)?$name:'';?>">
					      	<?php if (!empty($nameError)): ?>
					      		<span class="help-inline"><?php echo $nameError;?></span>
					      	<?php endif; ?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($addressError)?'error':'';?>">
					    <label class="control-label">Address</label>
					    <div class="controls">
					      	<input name="address" type="text" placeholder="Address" value="<?php echo !empty($address)?$address:'';?>">
					      	<?php if (!empty($addressError)): ?>
					      		<span class="help-inline"><?php echo $addressError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($phoneError)?'error':'';?>">
					    <label class="control-label">Phone Number</label>
					    <div class="controls">
					      	<input name="phone" type="text" placeholder="PhoneNumber" value="<?php echo !empty($phone)?$phone:'';?>">
					      	<?php if (!empty($phoneError)): ?>
					      		<span class="help-inline"><?php echo $phoneError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($mailError)?'error':'';?>">
					    <label class="control-label">Email ID</label>
					    <div class="controls">
					      	<input name="mail" type="text"  placeholder="Mail id" value="<?php echo !empty($mail)?$mail:'';?>">
					      	<?php if (!empty($mailError)): ?>
					      		<span class="help-inline"><?php echo $mailError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($HospitalError)?'error':'';?>">
					    <label class="control-label">Name of Hospital</label>
					    <div class="controls">
					      	<input name="Hospital" type="text"  placeholder="Name of Hospital" value="<?php echo !empty($Hospital)?$Hospital:'';?>">
					      	<?php if (!empty($HospitalError)): ?>
					      		<span class="help-inline"><?php echo $HospitalError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($DieaseError)?'error':'';?>">
					    <label class="control-label">Disease Handled</label>
					    <div class="controls">
					      	<input name="Diease" type="text"  placeholder="Disease Handled" value="<?php echo !empty($Diease)?$Diease:'';?>">
					      	<?php if (!empty($DieaseError)): ?>
					      		<span class="help-inline"><?php echo $DieaseError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($roleError)?'error':'';?>">
					    <label class="control-label">Role</label>
					    <div class="controls">
					      	<input name="role" type="text"  placeholder="Role" value="<?php echo !empty($role)?$role:'';?>">
					      	<?php if (!empty($roleError)): ?>
					      		<span class="help-inline"><?php echo $roleError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>


					 
						<br>
					  <div class="form-actions">
						  <button type="submit" class="btn btn-success">Update</button>
						  <a class="btn btn-success" href="admin_home.php">Back</a>
						</div>
					</form>
                           </div>
                    </div>

               </div>
            
           </div> <!-- /container -->




<!-- Copyright -->
  <footer class="site-footer">
    <div class="copyright">
        <div class="container">
          <div class="row">
            <div class="col-sm-4">
              <div class="copy-text">
                <p>All Rights Reserved | Copyright 2016 © <strong><a href="http://emr.smartideasinc.in/">EMR Med Services</a></strong> Developed By <strong><a href="http://smartideasinc.in/">SmartIdeas Inc</a></strong></p>
              </div>
            </div>
            <div class="col-sm-5">
              <div class="footer-menu pull-right">
                <ul>
                  <li><a href="index.html">Home</a></li>
                  <li><a href="#about">About</a></li>
                  <li><a href="#features">Services</a></li>
                  <li><a href="#faq">Faq</a></li>
                  <li><a href="#pricing">Pricing</a></li>
                  <li><a href="#blog">Blog</a></li>
                  <li><a href="#contact-us">Contact</a></li>
                </ul>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="social">
                <ul>
                  <li><a href="https://www.facebook.com/smartideasinc/"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="https://twitter.com/smartideas013"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="https://www.linkedin.com/company/smart-ideas-inc"><i class="fa fa-linkedin"></i></a></li>
                  <li><a href="info@smartideasinc.in"><i class="fa fa-google-plus"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
  </footer>
<!-- footer -->

</body>
</html>
