<?php 

session_start();
	require 'database-config.php';
        
            $link = mysql_connect($host, $user, $pass);
    if(!$link) {
        die('Failed to connect to server: ' . mysql_error());
    }
    
    //Select database
    $db = mysql_select_db($database);
    if(!$db) {
        die("Unable to select database");

    }
 
	require 'database.php';

	$id = null;
	if ( !empty($_GET['id'])) {
		$id = $_REQUEST['id'];
	}
	
	if ( null==$id ) {
		header("Location: patient_detail1.php");
	}
	
	if ( !empty($_POST)) {
		// keep track validation errors
		$nameError = null;
		$addressError = null;
		$refbyError = null;
		$mobileError = null;
		$dobError = null;
		$date_reportError = null;
		$heightError = null;
		$weightError = null;
		$ageError = null;
		$genderError = null;
		$diagnosisError = null;
		$HgA1c_DateError = null;
		$HgA1cError = null;
		$cholesterol_dateError = null;
		$LDL_cError = null;
		$HDLError = null;
		$TrigError = null;
		$commentsError = null;
		$interpretationError = null;
		$testError = null;
		
		// keep track post values
		$name = $_POST['name'];
		$address = $_POST['address'];
		$refby = $_POST['refby'];
		$mobile = $_POST['mobile'];
		$dob = $_POST['dob'];
		$date_report = $_POST['date_report'];
		$height = $_POST['height'];
		$weight = $_POST['weight'];
		$age = $_POST['age'];
		$gender = $_POST['gender'];
		$diagnosis = $_POST['diagnosis'];
		$HgA1c_Date = $_POST['HgA1c_Date'];
		$HgA1c = $_POST['HgA1c'];
		$cholesterol_date = $_POST['cholesterol_date'];
		$LDL_c = $_POST['LDL_c'];
		$HDL = $_POST['HDL'];
		$Trig = $_POST['Trig'];
		$comments = $_POST['comments'];
		$interpretation = $_POST['interpretation'];
		$test = $_POST['test'];
		
		
		
		
		// validate input
		$valid = true;
		if (empty($name)) {
			$nameError = 'Please enter Name';
			$valid = false;
		}
		
		if (empty($address)) {
			$addressError = 'Please enter address Address';
			$valid = false;
		} 

		if (empty($refby)) {
			$refbyError = 'Please enter Ref By';
			$valid = false;
		}
		
		if (empty($mobile)) {
			$mobileError = 'Please enter Mobile Number';
			$valid = false;
		}

		if (empty($dob)) {
			$dobError = 'Please enter Date Of Birth';
			$valid = false;
		}

		if (empty($date_report)) {
			$date_reportError = 'Please enter Date of Report';
			$valid = false;
		}

		if (empty($height)) {
			$heightError = 'Please enter Height';
			$valid = false;
		}

		if (empty($weight)) {
			$weightError = 'Please enter Weight';
			$valid = false;
		}

		if (empty($age)) {
			$ageError = 'Please enter Age';
			$valid = false;
		}

		if (empty($gender)) {
			$genderError = 'Please enter Gender';
			$valid = false;
		}

		if (empty($diagnosis)) {
			$diagnosisError = 'Please enter diagnosis Detail';
			$valid = false;
		}

		if (empty($HgA1c_Date)) {
			$HgA1c_DateError = 'Please enter HgA1c_Date';
			$valid = false;
		}

		if (empty($HgA1c)) {
			$HgA1cError = 'Please enter HgA1c';
			$valid = false;
		}

		if (empty($cholesterol_date)) {
			$cholesterol_dateError = 'Please enter cholesterol_date';
			$valid = false;
		}

		if (empty($LDL_c)) {
			$LDL_cError = 'Please enter LDL_c';
			$valid = false;
		}

		if (empty($HDL)) {
			$HDLError = 'Please enter HDL';
			$valid = false;
		}

		if (empty($Trig)) {
			$TrigError = 'Please enter Trig';
			$valid = false;
		}

		if (empty($comments)) {
			$commentsError = 'Please enter comments';
			$valid = false;
		}

		if (empty($interpretation)) {
			$interpretationError = 'Please enter interpretation';
			$valid = false;
		}



		
		// update data
		if ($valid) {
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE patient  set name = ?, address = ?, refby = ?,mobile =?, dob =?, date_report =?, height =?, weight =?, age =?, gender =?, diagnosis =?, HgA1c_Date =?, HgA1c =?, cholesterol_date =?, LDL_c =?, HDL =?, Trig =?, comments =?, interpretation =? WHERE id = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($name,$address,$refby,$mobile,$dob,$date_report,$height,$weight,$age,$gender,$diagnosis,$HgA1c_Date,$HgA1c,$cholesterol_date,$LDL_c,$HDL,$Trig,$comments,$interpretation,$id));
			Database::disconnect();
			header("Location: hospital_home.php");
		}
	} else {
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "SELECT * FROM patient where id = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($id));
		$data = $q->fetch(PDO::FETCH_ASSOC);
		$name = $data['name'];
		$address = $data['address'];
		$refby = $data['refby'];
		$mobile = $data['mobile'];
		$dob = $data['dob'];
		$date_report = $data['date_report'];
		$height = $data['height'];
		$weight = $data['weight'];
		$age = $data['age'];
		$gender = $data['gender'];
		$diagnosis = $data['diagnosis'];
		$HgA1c_Date = $data['HgA1c_Date'];
		$HgA1c = $data['HgA1c'];
		$cholesterol_date = $data['cholesterol_date'];
		$LDL_c = $data['LDL_c'];
		$HDL = $data['HDL'];
		$Trig = $data['Trig'];
		$comments = $data['comments'];
		$interpretation = $data['interpretation'];
		$test = $data['test'];
		
		Database::disconnect();
	}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>EMR MED REPORT</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->
        
        <!-- Font -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,700,600italic,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <!-- Font -->
        
        
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/responsive.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
</head>

<body>
<!-- Navigation -->
<div class="main-menu">
                <div class="navbar-wrapper">
                    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                        <div class="container">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle Navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                
                                <a href="#" class="navbar-brand"><img src="img/logo.png" alt="Logo" /></a>                          
                            </div>
                            
                            <div class="navbar-collapse collapse">
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="index.html">Home</a></li>
                                    <li><a href="patient_detail1.php">HOspital Home</a></li>
                                    <li><a href="logout.php">Logout</a></li>
                                </ul>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
 

<!-- Navigation ends -->

<br>
<br>
    <div class="container">
     
      <div class="col-md-6 col-md-offset-3">

                  <h4></span>Read a Patient Detail    <span class="glyphicon glyphicon-user"></h4>
                  <br/>
                            <div class="block-margin-top">
    		
	    			<form class="form-horizontal" action="update_hospital.php?id=<?php echo $id?>" method="post">
					  <div class="control-group <?php echo !empty($nameError)?'error':'';?>">
					    <label class="control-label">Name</label>
					    <div class="controls">
					      	<input name="name" type="text"  placeholder="Name" value="<?php echo !empty($name)?$name:'';?>">
					      	<?php if (!empty($nameError)): ?>
					      		<span class="help-inline"><?php echo $nameError;?></span>
					      	<?php endif; ?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($addressError)?'error':'';?>">
					    <label class="control-label">Address</label>
					    <div class="controls">
					      	<input name="address" type="text" placeholder="Address" value="<?php echo !empty($address)?$address:'';?>">
					      	<?php if (!empty($addressError)): ?>
					      		<span class="help-inline"><?php echo $addressError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($refbyError)?'error':'';?>">
					    <label class="control-label">Ref By</label>
					    <div class="controls">
					      	<input name="refby" type="text" placeholder="RefBy" value="<?php echo !empty($refby)?$refby:'';?>">
					      	<?php if (!empty($refbyError)): ?>
					      		<span class="help-inline"><?php echo $refbyError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($mobileError)?'error':'';?>">
					    <label class="control-label">Mobile Number</label>
					    <div class="controls">
					      	<input name="mobile" type="text"  placeholder="Mobile Number" value="<?php echo !empty($mobile)?$mobile:'';?>">
					      	<?php if (!empty($mobileError)): ?>
					      		<span class="help-inline"><?php echo $mobileError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($dobError)?'error':'';?>">
					    <label class="control-label">Date Of Birth</label>
					    <div class="controls">
					      	<input name="dob" type="text"  placeholder="Date Of Birth" value="<?php echo !empty($dob)?$dob:'';?>">
					      	<?php if (!empty($dobError)): ?>
					      		<span class="help-inline"><?php echo $dobError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($date_reportError)?'error':'';?>">
					    <label class="control-label">Date Of Report</label>
					    <div class="controls">
					      	<input name="date_report" type="text"  placeholder="Date Of Report" value="<?php echo !empty($date_report)?$date_report:'';?>">
					      	<?php if (!empty($date_reportError)): ?>
					      		<span class="help-inline"><?php echo $date_reportError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($heightError)?'error':'';?>">
					    <label class="control-label">Height</label>
					    <div class="controls">
					      	<input name="height" type="text"  placeholder="Height" value="<?php echo !empty($height)?$height:'';?>">
					      	<?php if (!empty($heightError)): ?>
					      		<span class="help-inline"><?php echo $heightError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($weightError)?'error':'';?>">
					    <label class="control-label">Weight</label>
					    <div class="controls">
					      	<input name="weight" type="text"  placeholder="Weight" value="<?php echo !empty($weight)?$weight:'';?>">
					      	<?php if (!empty($weightError)): ?>
					      		<span class="help-inline"><?php echo $weightError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($ageError)?'error':'';?>">
					    <label class="control-label">Age</label>
					    <div class="controls">
					      	<input name="age" type="text"  placeholder="Age" value="<?php echo !empty($age)?$age:'';?>">
					      	<?php if (!empty($ageError)): ?>
					      		<span class="help-inline"><?php echo $ageError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($genderError)?'error':'';?>">
					    <label class="control-label">Gender</label>
					    <div class="controls">
					      	<input name="gender" type="text"  placeholder="Gender" value="<?php echo !empty($gender)?$gender:'';?>">
					      	<?php if (!empty($genderError)): ?>
					      		<span class="help-inline"><?php echo $genderError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($diagnosisError)?'error':'';?>">
					    <label class="control-label">Diagnosis</label>
					    <div class="controls">
					      	<input name="diagnosis" type="text"  placeholder="diagnosis" value="<?php echo !empty($diagnosis)?$diagnosis:'';?>">
					      	<?php if (!empty($diagnosisError)): ?>
					      		<span class="help-inline"><?php echo $diagnosisError;?></span>
					      	<?php endif;?>
					    </div>

					  </div>
					   <div class="control-group <?php echo !empty($HgA1c_DateError)?'error':'';?>">
					    <label class="control-label">HgA1c_Date</label>
					    <div class="controls">
					      	<input name="HgA1c_Date" type="date"  placeholder="HgA1c_Date" value="<?php echo !empty($HgA1c_Date)?$HgA1c_Date:'';?>">
					      	<?php if (!empty($HgA1c_DateError)): ?>
					      		<span class="help-inline"><?php echo $HgA1c_DateError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					   <div class="control-group <?php echo !empty($HgA1cError)?'error':'';?>">
					    <label class="control-label">HgA1c</label>
					    <div class="controls">
					      	<input name="HgA1c" type="text"  placeholder="HgA1c" value="<?php echo !empty($HgA1c)?$HgA1c:'';?>">
					      	<?php if (!empty($HgA1cError)): ?>
					      		<span class="help-inline"><?php echo $HgA1cError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					   <div class="control-group <?php echo !empty($cholesterol_dateError)?'error':'';?>">
					    <label class="control-label">cholesterol_date</label>
					    <div class="controls">
					      	<input name="cholesterol_date" type="date"  placeholder="cholesterol_date" value="<?php echo !empty($cholesterol_date)?$cholesterol_date:'';?>">
					      	<?php if (!empty($cholesterol_dateError)): ?>
					      		<span class="help-inline"><?php echo $cholesterol_dateError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					   <div class="control-group <?php echo !empty($LDL_cError)?'error':'';?>">
					    <label class="control-label">LDL_c</label>
					    <div class="controls">
					      	<input name="LDL_c" type="text"  placeholder="LDL_c" value="<?php echo !empty($LDL_c)?$LDL_c:'';?>">
					      	<?php if (!empty($LDL_cError)): ?>
					      		<span class="help-inline"><?php echo $LDL_cError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					   <div class="control-group <?php echo !empty($HDLError)?'error':'';?>">
					    <label class="control-label">HDL</label>
					    <div class="controls">
					      	<input name="HDL" type="text"  placeholder="HDL" value="<?php echo !empty($HDL)?$HDL:'';?>">
					      	<?php if (!empty($HDLError)): ?>
					      		<span class="help-inline"><?php echo $HDLError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($TrigError)?'error':'';?>">
					    <label class="control-label">Trig</label>
					    <div class="controls">
					      	<input name="Trig" type="text"  placeholder="Trig" value="<?php echo !empty($Trig)?$Trig:'';?>">
					      	<?php if (!empty($TrigError)): ?>
					      		<span class="help-inline"><?php echo $TrigError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>
					  <div class="control-group <?php echo !empty($testError)?'error':'';?>">
					    <label class="control-label">Test Assign</label>
					    <div class="controls">
					      	<input name="Trig" type="text"  placeholder="test" value="<?php echo !empty($test)?$test:'';?>" disabled>
					      	<?php if (!empty($testError)): ?>
					      		<span class="help-inline"><?php echo $testError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					   <div class="control-group <?php echo !empty($commentsError)?'error':'';?>">
					    <label class="control-label">Comments</label>
					    <div class="controls">
					      	<input name="comments" type="text" placeholder="Comments" value="<?php echo !empty($comments)?$comments:'';?>">
					      	<?php if (!empty($commentsError)): ?>
					      		<span class="help-inline"><?php echo $commentsError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($interpretationError)?'error':'';?>">
					    <label class="control-label">Interpretation</label>
					    <div class="controls">
					      	<input name="interpretation" type="text" placeholder="Interpretation" value="<?php echo !empty($interpretation)?$Interpretation:'';?>">
					      	<?php if (!empty($interpretationError)): ?>
					      		<span class="help-inline"><?php echo $interpretationError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					 
						<br>
					  <div class="form-actions">
						  <button type="submit" class="btn btn-success">Update</button>
						  <a class="btn btn-success" href="patient_detail1.php">Back</a>
						</div>
					</form>
                           </div>
                    </div>

               </div>
            
           </div> <!-- /container -->
<br>
<br>
  <div class="copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="copy-text">
                                <p>All Rights Reserved | Copyright 2016 © <strong><a href="http://www.pfind.com/goodies/bizium/">EMR Med Services</a></strong> Developed By <strong><a href="http://smartideasinc.in/">SmartIdeas Inc</a></strong></p>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="footer-menu pull-right">
                                <ul>
                                    <li><a href="index.html">Home</a></li>
                                    <li><a href="#">About</a></li>
                                    <li><a href="#">Services</a></li>
                                    <li><a href="#">Faq</a></li>
                                    <li><a href="#">Pricing</a></li>
                                    <li><a href="#">Blog</a></li>
                                    <li><a href="#">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="social">
                                <ul>
                                    <li><a href="https://www.facebook.com/smartideasinc/"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="https://twitter.com/smartideas013"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="https://www.linkedin.com/company/smart-ideas-inc"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="info@smartideasinc.in"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
        <!-- footer -->


</body>
</html>
