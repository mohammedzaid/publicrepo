<?php 

session_start();
	require 'database-config.php';
        
            $link = mysql_connect($host, $user, $pass);
    if(!$link) {
        die('Failed to connect to server: ' . mysql_error());
    }
    
    //Select database
    $db = mysql_select_db($database);
    if(!$db) {
        die("Unable to select database");

    }
 
	require 'database.php';

	$id = null;
	if ( !empty($_GET['id'])) {
		$id = $_REQUEST['id'];
	}
	
	if ( null==$id ) {
		header("Location: doctor_home.php");
	}
	
	if ( !empty($_POST)) {
		// keep track validation errors
		$nameError = null;
		$addressError = null;
		$refbyError = null;
		$mobileError = null;
		$dobError = null;
		$date_reportError = null;
		$heightError = null;
		$weightError = null;
		$ageError = null;
		$genderError = null;
		$testError = null;
		
		// keep track post values
		$name = $_POST['name'];
		$address = $_POST['address'];
		$refby = $_POST['refby'];
		$mobile = $_POST['mobile'];
		$dob = $_POST['dob'];
		$date_report = $_POST['date_report'];
		$height = $_POST['height'];
		$weight = $_POST['weight'];
		$age = $_POST['age'];
		$gender = $_POST['gender'];
		$test = $_POST['test'];
		
		
		
		
		// validate input
		$valid = true;
		
		if (empty($test)) {
			$testError = 'Please select a Test';
			$valid = false;
		}



		
		// update data
		if ($valid) {
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE patient  set test = ? WHERE id = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($test,$id));
			Database::disconnect();
			header("Location: doctor_home.php");
		}
	} else {
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "SELECT * FROM patient where id = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($id));
		$data = $q->fetch(PDO::FETCH_ASSOC);
		$name = $data['name'];
		$address = $data['address'];
		$refby = $data['refby'];
		$mobile = $data['mobile'];
		$dob = $data['dob'];
		$date_report = $data['date_report'];
		$height = $data['height'];
		$weight = $data['weight'];
		$age = $data['age'];
		$gender = $data['gender'];
		
		
		Database::disconnect();
	}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>EMR MED REPORT</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->
        
        <!-- Font -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,700,600italic,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <!-- Font -->
        
        
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/responsive.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
</head>

<body>
<!-- Navigation -->
<div class="main-menu">
                <div class="navbar-wrapper">
                    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                        <div class="container">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle Navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                
                                <a href="#" class="navbar-brand"><img src="img/logo.png" alt="Logo" /></a>                          
                            </div>
                            
                            <div class="navbar-collapse collapse">
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="index.html">Home</a></li>
                                    <li><a href="doctor_home.php">Doctor Home</a></li>
                                    <li><a href="logout.php">Logout</a></li>
                                </ul>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
 

<!-- Navigation ends -->

<br>
<br>
    <div class="container">
     
      <div class="col-md-6 col-md-offset-3">

                  <h4></span>Read a Patient Detail    <span class="glyphicon glyphicon-user"></h4>
                  <br/>
                            <div class="block-margin-top">
    		
	    			<form class="form-horizontal" action="test_assign.php?id=<?php echo $id?>" method="post">
					  <div class="control-group <?php echo !empty($nameError)?'error':'';?>">
					    <label class="control-label">Name</label>
					    <div class="controls">
					      	<input name="name" type="text"  placeholder="Name" value="<?php echo !empty($name)?$name:'';?>" disabled>
					      	<?php if (!empty($nameError)): ?>
					      		<span class="help-inline"><?php echo $nameError;?></span>
					      	<?php endif; ?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($addressError)?'error':'';?>">
					    <label class="control-label">Address</label>
					    <div class="controls">
					      	<input name="address" type="text" placeholder="Address" value="<?php echo !empty($address)?$address:'';?>"disabled>
					      	<?php if (!empty($addressError)): ?>
					      		<span class="help-inline"><?php echo $addressError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($refbyError)?'error':'';?>">
					    <label class="control-label">Ref By</label>
					    <div class="controls">
					      	<input name="refby" type="text" placeholder="RefBy" value="<?php echo !empty($refby)?$refby:'';?>" disabled>
					      	<?php if (!empty($refbyError)): ?>
					      		<span class="help-inline"><?php echo $refbyError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($mobileError)?'error':'';?>">
					    <label class="control-label">Mobile Number</label>
					    <div class="controls">
					      	<input name="mobile" type="text"  placeholder="Mobile Number" value="<?php echo !empty($mobile)?$mobile:'';?>" disabled>
					      	<?php if (!empty($mobileError)): ?>
					      		<span class="help-inline"><?php echo $mobileError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($dobError)?'error':'';?>">
					    <label class="control-label">Date Of Birth</label>
					    <div class="controls">
					      	<input name="dob" type="text"  placeholder="Date Of Birth" value="<?php echo !empty($dob)?$dob:'';?>" disabled>
					      	<?php if (!empty($dobError)): ?>
					      		<span class="help-inline"><?php echo $dobError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($date_reportError)?'error':'';?>">
					    <label class="control-label">Date Of Report</label>
					    <div class="controls">
					      	<input name="date_report" type="text"  placeholder="Date Of Report" value="<?php echo !empty($date_report)?$date_report:'';?>" disabled>
					      	<?php if (!empty($date_reportError)): ?>
					      		<span class="help-inline"><?php echo $date_reportError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($heightError)?'error':'';?>">
					    <label class="control-label">Height</label>
					    <div class="controls">
					      	<input name="height" type="text"  placeholder="Height" value="<?php echo !empty($height)?$height:'';?>" disabled>
					      	<?php if (!empty($heightError)): ?>
					      		<span class="help-inline"><?php echo $heightError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($weightError)?'error':'';?>">
					    <label class="control-label">Weight</label>
					    <div class="controls">
					      	<input name="weight" type="text"  placeholder="Weight" value="<?php echo !empty($weight)?$weight:'';?>" disabled>
					      	<?php if (!empty($weightError)): ?>
					      		<span class="help-inline"><?php echo $weightError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($ageError)?'error':'';?>">
					    <label class="control-label">Age</label>
					    <div class="controls">
					      	<input name="age" type="text"  placeholder="Age" value="<?php echo !empty($age)?$age:'';?>" disabled>
					      	<?php if (!empty($ageError)): ?>
					      		<span class="help-inline"><?php echo $ageError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($genderError)?'error':'';?>">
					    <label class="control-label">Gender</label>
					    <div class="controls">
					      	<input name="gender" type="text"  placeholder="Gender" value="<?php echo !empty($gender)?$gender:'';?>" disabled>
					      	<?php if (!empty($genderError)): ?>
					      		<span class="help-inline"><?php echo $genderError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

					  <div class="control-group <?php echo !empty($testError)?'error':'';?>">
					    <label class="control-label">Test Assign</label>
					    <div class="controls">
					    <select style="width: 150px;" name="test">
					            <option value="">Select...</option>
					            

					            <option style="width: 150px;" value="Glucose Test">Glucose Test</option>
					            <option style="width: 150px;" value="blood Test">Blood Test</option>
					    </select>
					    </div>
					    </div>

					  
					 
						<br>
					  <div class="form-actions">
						  <button type="submit" class="btn btn-success">Update</button>
						  <a class="btn btn-success" href="doctor_home.php">Back</a>
						</div>
					</form>
                           </div>
                    </div>

               </div>
            
           </div> <!-- /container -->
<br>
<br>
  <div class="copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="copy-text">
                                <p>All Rights Reserved | Copyright 2016 © <strong><a href="http://www.pfind.com/goodies/bizium/">EMR Med Services</a></strong> Developed By <strong><a href="http://smartideasinc.in/">SmartIdeas Inc</a></strong></p>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="footer-menu pull-right">
                                <ul>
                                    <li><a href="index.html">Home</a></li>
                                    <li><a href="#">About</a></li>
                                    <li><a href="#">Services</a></li>
                                    <li><a href="#">Faq</a></li>
                                    <li><a href="#">Pricing</a></li>
                                    <li><a href="#">Blog</a></li>
                                    <li><a href="#">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="social">
                                <ul>
                                    <li><a href="https://www.facebook.com/smartideasinc/"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="https://twitter.com/smartideas013"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="https://www.linkedin.com/company/smart-ideas-inc"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="info@smartideasinc.in"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
        <!-- footer -->


</body>
</html>
