-- MySQL dump 10.13  Distrib 5.5.53, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: medical
-- ------------------------------------------------------
-- Server version	5.5.53-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `mail_id` varchar(40) NOT NULL,
  `Hospital` varchar(30) NOT NULL,
  `Diease` varchar(20) NOT NULL,
  `logo` varchar(20) NOT NULL,
  `role` varchar(100) NOT NULL,
  `code` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login`
--

LOCK TABLES `login` WRITE;
/*!40000 ALTER TABLE `login` DISABLE KEYS */;
INSERT INTO `login` VALUES (1,'shafi','bangalore','9036931082','admin','admin123','uzma.iqbal@smartideasinc.in','NA','','in.jpg','admin','kzjNu3vD'),(2,'UZMA','bangalore','9036931082','hospital','hospital','abc@smartideasinc.in','uzma','NA','slider1.jpg','Hospital','123451'),(3,'zaid','bangalore','9036931082','doctor','doctor','zaid@gmail.com','uzma','Thyroid','bilal3.jpg','doctor','123451'),(4,'abc','abc','9036931082','hospital1','hospital1','123@gmail.com','abc','NA','slider1.jpg','Hospital','123451'),(5,'doctor1','bangalore','9036931082','doctor1','doctor1','1234@gmail.com','abc','Diabetes','slider1.jpg','doctor','123451');
/*!40000 ALTER TABLE `login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient`
--

DROP TABLE IF EXISTS `patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `refby` varchar(50) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `date_report` date NOT NULL,
  `height` varchar(30) NOT NULL,
  `weight` varchar(30) NOT NULL,
  `age` int(10) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `disease` varchar(30) NOT NULL,
  `Hospital` varchar(40) NOT NULL,
  `diagnosis` varchar(50) NOT NULL,
  `HgA1c_Date` date NOT NULL,
  `HgA1c` float(5,3) NOT NULL,
  `cholesterol_date` date NOT NULL,
  `LDL_c` int(10) NOT NULL,
  `HDL` int(10) NOT NULL,
  `Trig` int(10) NOT NULL,
  `comments` varchar(300) NOT NULL,
  `interpretation` varchar(500) NOT NULL,
  `mail_id` varchar(40) NOT NULL,
  `logo` varchar(40) NOT NULL,
  `test` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient`
--

LOCK TABLES `patient` WRITE;
/*!40000 ALTER TABLE `patient` DISABLE KEYS */;
INSERT INTO `patient` VALUES (1,'Ahana Shanbog','#301,Tata Berlon Villa,MG Road,Bangalore-008','','9443256780','1994-10-20','2016-09-10','150cm','50kg',25,'Female','diabetes','','Diabetes,Dyslipidemia','2016-09-12',12.900,'2016-09-12',190,140,330,'','','mhdzaid17@gmail.com','logo_example.jpg',''),(2,'chaithra','chennai','zaid@gmail.com','9036931082','1990-11-12','2016-11-11','145','56',24,'Female','Diabetes','uzma','abx','0000-00-00',0.000,'0000-00-00',0,0,0,'ghg','ghg','','slider1.jpg','Glucose Test'),(3,'patient1','BANGALORE','1234@gmail.com','9036931082','1990-11-12','2016-11-11','145','56',24,'Female','Diabetes','abc','','0000-00-00',0.000,'0000-00-00',0,0,0,'','','','slider1.jpg',''),(4,'1','1','zaid@gmail.com','9036931082','1990-11-12','2016-11-11','145','56',24,'Female','Diabetes','uzma','','0000-00-00',0.000,'0000-00-00',0,0,0,'','','','bilal3.jpg','');
/*!40000 ALTER TABLE `patient` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-16 14:51:44
